<?php

session_start();

if (isset($_POST['submitPersonalData'])) {

    $_SESSION['firstname'] = $_POST['firstname'];
    $_SESSION['surname'] = $_POST['surname'];
    $_SESSION['email'] = $_POST['password'];
    $_SESSION['password'] = $_POST['password'];

    header('Location: form-address.php', true);
    exit();
}


?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <form action="form-personal-data.php" method="post" class="form-horizontal">

            <div class="form-group">
                <label for="inputFirstName" class="control-label col-sm-2">Vorname</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Vorname" name="firstname" id="inputFirstName">
                </div>
            </div>

            <div class="form-group">
                <label for="inputSurname" class="control-label col-sm-2">Nachname</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Nachname" name="surname" id="inputSurname">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="control-label col-sm-2">E-Mail</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="E-Mail" name="email" id="inputEmail">
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword" class="control-label col-sm-2">Passwort</label>
                <div class="col-sm-10">
                    <input type="password" placeholder="Passwort" class="form-control" name="password" id="inputPassword">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn  btn-primary" type="submit" name="submitPersonalData">
                        weiter
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>