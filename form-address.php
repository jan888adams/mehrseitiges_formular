<?php
session_start();

if (isset($_POST['submitAddresslData'])) {

    $_SESSION['address_street'] = $_POST['address_street'];
    $_SESSION['address_city'] = $_POST['address_city'];
    $_SESSION['address_country'] = $_POST['address_country'];

    header('Location: form-company.php', true);
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Anschrift</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <form action="form-address.php" method="post" class="form-horizontal">

            <div class="form-group">
                <label for="inputStreet" class="control-label col-sm-2">Strasse</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Strasse" name="address_street" id="inputStreet">
                </div>
            </div>

            <div class="form-group">
                <label for="inputCity" class="control-label col-sm-2">PLZ / Ort</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="PLZ und Ort" name="address_city" id="inputCity">
                </div>
            </div>

            <div class="form-group">
                <label for="inputCountry" class="control-label col-sm-2">Land</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Land" name="address_country" id="inputCountry">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn  btn-primary" type="submit" name="submitAddresslData">weiter</button>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>